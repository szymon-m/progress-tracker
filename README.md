# Progress tracker

*Learning process tracking app, for scheduling, metring, visualizing to-do's, building confidence in learned areas*




- 31.01.2021 - basic repository configuration - virtualenv setting up, creating minimal Django project and basic functional test - extra : linked with issue#1 (added in submit message)

- 01.02.2021 - Neo4j branch - basic setting up and transaction tests - with working local db

- 03.02.2021 
  - Neo4j branch - neo4j tests refactoring - general cleanup, using pytest fixtures and conftest.py, lambdas in write/readtransaction
  - master branch - basic django setup #3  
