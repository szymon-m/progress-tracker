from selenium import webdriver

def test_smoke_with_pytest():

    browser = webdriver.Firefox()
    browser.get('http://localhost:8000')
    assert 'Django' in browser.title

    browser.quit()
